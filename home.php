<?php
/**
 * Home Template File
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
 */

$context = Timber::get_context();
$context['post'] = Timber::get_post( get_option('page_for_posts') );
$context['posts'] = Timber::get_posts('posts_per_page=-1');
$context['hero_image'] = $context['post']->thumbnail();
$templates = array( 'blog.twig' );
$context['authors'] = array_map( 
	function($user){
		return new TimberUser($user);
	}, 
	get_users( array( 'number' => 100 ) )
);

$context['categories'] = get_categories();

Timber::render( $templates, $context );