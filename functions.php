<?php

if ( ! class_exists( 'Timber' ) ) {
	add_action( 'admin_notices', function() {
			echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url( admin_url( 'plugins.php#timber' ) ) . '">' . esc_url( admin_url( 'plugins.php' ) ) . '</a></p></div>';
		} );
	return;
}

Timber::$dirname = array('templates', 'views');

class WeAre502 extends TimberSite {

	function __construct() {
		// add_theme_support( 'post-formats' );
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'menus' );
		add_theme_support( 'title-tag' );

		add_action( 'after_setup_theme', array( $this, 'after_setup_theme' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
		add_action( 'admin_head', array( $this, 'admin_head_css' ) );
		add_action( 'init', array( $this, 'register_post_types' ) );
		
		add_filter( 'timber_context', array( $this, 'add_to_context' ) );
		add_filter( 'mce_buttons_2', array( $this, 'tiny_mce_buttons' ) );
		add_filter( 'tiny_mce_before_init', array( $this, 'tiny_mce_insert_formats' ) );
		add_filter( 'dashboard_glance_items', array( $this, 'dashboard_glance_items' ) );
		add_filter( 'gform_enable_field_label_visibility_settings', '__return_true' );
		add_filter('wp_nav_menu_objects', array( $this, 'wp_nav_menu_objects' ), 10, 2);

		add_filter( 'nav_menu_link_attributes', function( $atts, $item, $args ){
			$text = get_field('hover_text', $item->ID );
			if ( $text ){
				$atts['data-hover'] = $text;
			}
			return $atts;
		}, 10, 3 );

		add_action( 'init', function(){
			add_editor_style( get_stylesheet_directory_uri() . "/static/scss/style.css" );
		} );

		parent::__construct();
	}

	function add_to_context( $context ) {
		$context['site'] = $this;
		$context['logo'] = trailingslashit( get_template_directory_uri() ) . 'static/images/logo.png';
		$context['year'] = date('Y');
		$context['options'] = get_fields('option');
		$context['is_home'] = is_home();
		$context['is_front_page'] = is_front_page();
		$context['csscache'] = filemtime(get_stylesheet_directory() . '/static/scss/style.css');
		$context['jscache'] = filemtime(get_stylesheet_directory() . '/static/js/site.js');
		$context['plugin_content'] = TimberHelper::ob_function( 'the_content' );
		$context['breadcrumbs'] = function_exists('yoast_breadcrumb') ? yoast_breadcrumb( '<div id="breadcrumbs">', '</div>', false ) : "";
		$context['blog_url'] = get_permalink( get_option('page_for_posts') );
		
		return $context;
	}

	function after_setup_theme(){
		register_nav_menu( 'primary', 'Main Navigation' );
		register_nav_menu( 'footer', 'Footer Navigation' );

		// register_sidebar( 'sidebar');
		// Images Sizes
		add_image_size( 'xlarge', 2400, 1500 );
	}

	function enqueue_scripts(){
		// Dependencies
		wp_enqueue_script('jquery');
		// Main JS
	}

	function admin_head_css(){
		?><style type="text/css">
			.mce-ico.fa { font-family: 'FontAwesome', 'Dashicons'; }
		</style><?php
	}

	function tiny_mce_buttons( $buttons ) {
		array_unshift( $buttons, 'styleselect' );
		return $buttons;
	}

	// Callback function to filter the MCE settings
	function tiny_mce_insert_formats( $init_array ) {  
		// Define the style_formats array
		$style_formats = array(  
			// Each array child is a format with it's own settings
			array(  
				'title'    => 'Button',  
				'selector' => 'a',  
				'classes'  => 'button',
				// font awesome must be available in the admin area to see the icon
				'icon'     => ' fa fa-hand-pointer-o'
			),
			array(  
				'title'    => 'Inline Quote',
				'classes'  => 'inline-quote',
				// font awesome must be available in the admin area to see the icon
				'icon'     => ' fa fa-quote-right'
			),
			array(  
				'title'    => 'First Letter',  
				'classes'  => 'first-letter',
				// font awesome must be available in the admin area to see the icon
				'icon'     => ' fa fa-quora'
			),
		);  
		// Insert the array, JSON ENCODED, into 'style_formats'
		$init_array['style_formats'] = json_encode( $style_formats );
		$init_array['body_class'] .= " content ";
		return $init_array;  
	  
	}

	function register_post_types(){
		include 'inc/post-type-work.php';
		include 'inc/post-type-staff.php';
		include 'inc/post-type-job.php';
		include 'inc/post-type-industry-work.php';
		include 'inc/gfield-weare502-fancy-checkbox.php';
		include 'inc/fancy-heading-module/fancy-heading.php';
	}

	function dashboard_glance_items( $items ){
		foreach ( get_post_types(array('public'=>true)) as $post_type ){
			$num_posts = wp_count_posts( $post_type );
			if ( $num_posts && $num_posts->publish ) {
				if ( 'post' == $post_type ) {
					continue;
				}
				if ( 'page' == $post_type ) {
					continue;
				}
				$post_type_object = get_post_type_object( $post_type );
				$text = _n( '%s ' . $post_type_object->labels->singular_name, '%s ' . $post_type_object->label, $num_posts->publish );
				$text = sprintf( $text, number_format_i18n( $num_posts->publish ) );
				if ( $post_type_object && current_user_can( $post_type_object->cap->edit_posts ) ) {
					$items[] = sprintf( '<a href="edit.php?post_type=%1$s">%2$s</a>', $post_type, $text );
				} else {
					$items[] = sprintf( '<span>%2$s</span>', $post_type, $text );
				}
			}
		}
		
		return $items;
	}

	function wp_nav_menu_objects( $items, $args ) {
		foreach( $items as &$item ) {
			$text = get_field('hover_text', $item);
			// append text
			if( $text ) {
				$item->title = '<span>' . $item->title . '</span>';	
			}

			$new_column = get_field('new_column', $item);
			if ( $new_column ){
				$item->classes[] = "new-column";
			}

			$header = get_field('header', $item);
			if ( $header ){
				$item->classes[] = "sub-menu-header";
			}
		}
		return $items;
	}

}

new WeAre502();

function weare502_render_primary_menu(){ // used in header.twig
	// Twig won't let us call functions with lots of arguments. This is easier!
	// This render method also works correctly with the customizer selective refresh
	wp_nav_menu( array(
		'theme_location' => 'primary',
		'container' => '',
		'menu_class' => 'nav-menu menu',
		'menu_id' => 'primary-menu'
	) );
}

function weare502_render_home_menu(){ // used in header.twig
	// Twig won't let us call functions with lots of arguments. This is easier!
	// This render method also works correctly with the customizer selective refresh
	wp_nav_menu( array(
		'theme_location' => 'primary',
		'container' => '',
		'menu_class' => 'nav-menu menu inline-flex-container flex-container--wrap',
		'menu_id' => 'front-page-menu'
	) );
}

if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Site Options',
		'menu_title'	=> 'Site Options',
		'menu_slug' 	=> 'site-options',
		// 'icon_url'		=> 'dashicons-warning',
		'capability'	=> 'edit_posts',
		// 'position'		=> 1,
		'redirect'		=> false
	));
	
}

function weare502_modify_footer_cta( $post, &$context ){
	if ( ! ($post instanceof TimberPost) ) {
		return;
	}

	$ft = $post->get_field('footer_cta_text');
	$flbt = $post->get_field('footer_link_button_text');
	$fl = $post->get_field('footer_link');

	if ( ! empty( $ft ) ){
		$context['options']['footer_cta_text'] = $ft;
	}

	if ( ! empty( $flbt ) ){
		$context['options']['footer_link_button_text'] = $flbt;
	}

	if ( ! empty( $fl ) ){
		$context['options']['footer_link'] = $fl;
	}
}

add_action( 'gform_field_standard_settings', 'weare502_image_url_setting', 10, 2 );
function weare502_image_url_setting( $position, $form_id ) {
    //create settings on position 25 (right after Field Label)
    if ( $position == 25 ) : ?>
        <li class="image_url_setting field_setting">
            <label class="section_label" for="field_weare502_image_url">
                <?php esc_html_e( 'Image URL', 'gravityforms' ); ?>
            </label>
            <input type="text" class="fieldwidth-3" id="field_weare502_image_url" onchange="SetFieldProperty('image_url', this.value)" placeholder="https://weare502.com/wp-content/uploads/..." />
            <p>Save the form to see the image preview.</p>
        </li>
	<?php endif;
}

function weare502_image_url_setting_js() {
?>

  <script type="text/javascript">
    /*
     * When the field settings are initialized, populate
     * the custom field setting.
     */
    jQuery(document).on('gform_load_field_settings', function(ev, field) {
      jQuery('#field_weare502_image_url').val(field.image_url || '');
    });
  </script>

<?php
}
add_action( 'gform_editor_js', 'weare502_image_url_setting_js' );