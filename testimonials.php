<?php require __DIR__ . '/template-parts/html-head.php'; ?>

<div id="page" class="site testimonials full-width">

    <a class="skip-link screen-reader-text" href="#content">Skip Links</a>

    <div class="hero" style="background-image: url('/static/images/midwest-brands-hero-bg.jpg');">

        <div class="hero__wrap">

			<?php require __DIR__ . '/template-parts/header.php'; ?>

            <header class="entry-header">

                <h1 class="entry-title heading heading--light heading--separator-after--white font-size--12" itemprop="headline">
                    Why
                    Midwest Brands?</h1>

                <p class="entry-subtitle entry-subtitle--light" itemprop="tagline">We love Midwest businesses because
                    they are the heart of America, both in location and values. By helping to tell these stories, we
                    help build the community that surrounds them.</p>

            </header>

            <a href="#main" class="hero__scroll-indicator scroll-to">
                <span class="scroll-indicator__text">Scroll</span>
                <span class="scroll-indicator__arrow"></span>
            </a>

        </div>

    </div>

    <div id="content" class="site-content">

        <div id="primary" class="content-area">


            <main id="main" class="site-main">

                <section class="section--full-width" itemscope="" itemtype="https://schema.org/CreativeWork">

                    <div class="section__wrap section__wrap--wide">

                        <img class="alignleft industry__image" src="{{ TimberImage( post.get_field('office_photo') ).src('large') }}" alt="TimberImage( post.get_field('office_photo') ).alt"/>

                        <h2 class="heading font-size--4">Our Industries</h2>

                        <p>The industries that we work with are a big part of who we are. We work with industries that
                            are
                            involved in their community and love the stories
                            they contribute as much as we do.</p>

                        <p>Truth be told, we work a lot like you work. We see a problem. We tackle the problem. We
                            repeat.
                            No jargon or proprietary formula needed. When looking for an agency, you need an approach
                            that
                            is real and relatable. Something human. Below is a list of the industries we work with:</p>

                        <ul class="industry__list flex-container flex-container--wrap">
                            <li class="industry__wrap flex-container flex-column">
                                <img class="aligncenter" src="/static/images/industries-agriculture.png" alt="Agriculture"/>
                                <h3 class="heading heading--orange">Agriculture</h3>
                            </li>
                            <li class="industry__wrap flex-container flex-column">
                                <img class="aligncenter" src="/static/images/industries-travel.png" alt="Travel"/>
                                <h3 class="heading heading--orange">Travel</h3>
                            </li>
                            <li class="industry__wrap flex-container flex-column">
                                <img class="aligncenter" src="/static/images/industries-non-profit.png" alt="Non-profit"/>
                                <h3 class="heading heading--orange">Non-profit</h3>
                            </li>
                            <li class="industry__wrap flex-container flex-column">
                                <img class="aligncenter" src="/static/images/industries-education.png" alt="Education"/>
                                <h3 class="heading heading--orange">Education</h3>
                            </li>
                            <li class="industry__wrap flex-container flex-column">
                                <img class="aligncenter" src="/static/images/industries-retail.png" alt="Retail"/>
                                <h3 class="heading heading--orange">Retail</h3>
                            </li>
                        </ul>

                    </div>

                </section>

                <section class="section--full-width section--background-image background-image--box-shadow margin-bottom--0" style="background-image:url('/static/images/testimonials-bg.jpg');" itemscope="" itemtype="https://schema.org/CreativeWork">

                    <div class="testimonials-slider__testimonial-quote">
                        <h3 class="heading heading--separator-after">“Their work was exceptional and their
                            attention
                            to detail
                            made every phase of the project seamless. They are trustworthy and capable. Whatever
                            the
                            need, I
                            believe they have the talent and capacity to  deliver results.”</h3>
                        <span class="quote-credits color--orange">Doug Hofbauer, Frontier Farm Credit</span>
                        <div class="margin-top--6">
                            <a href="#" class="button">View Project</a>
                        </div>
                    </div>

                </section>

                <section class="section--half-width section--background-image--right" style="background-image:url('/static/images/testimonials-bg-1.jpg');" itemscope="" itemtype="https://schema.org/CreativeWork">

                    <div class="section__wrap">

                        <div class="testimonials-slider__testimonial-quote">
                            <h3 class="heading heading--separator-after">“Their work was exceptional and their
                                attention
                                to detail
                                made every phase of the project seamless. They are trustworthy and capable. Whatever
                                the
                                need, I
                                believe they have the talent and capacity to  deliver results.”</h3>
                            <span class="quote-credits color--orange">Doug Hofbauer, Frontier Farm Credit</span>
                            <div class="margin-top--6">
                                <a href="#" class="button button--outline">View Project</a>
                            </div>
                        </div>

                    </div>

                </section>

                <section class="section--testimonial-bg background-image--box-shadow margin-bottom--0" style="background-image:url('/static/images/testimonials-bg-2.jpg');" itemscope="" itemtype="https://schema.org/CreativeWork">

                    <div class="testimonials__block--right testimonials-slider__testimonial-quote">
                        <h3 class="heading heading--separator-after">“Their work was exceptional and their
                            attention
                            to detail
                            made every phase of the project seamless. They are trustworthy and capable. Whatever
                            the
                            need, I
                            believe they have the talent and capacity to  deliver results.”</h3>
                        <span class="quote-credits color--orange">Doug Hofbauer, Frontier Farm Credit</span>
                    </div>

                </section>

                <section class="section--full-width" itemscope="" itemtype="https://schema.org/CreativeWork">

                    <div class="section__wrap section__wrap--wide">

                        <div class="grid grid--2 grid-gutter--30 testimonials__project-grid">

                            <div class="entry grid__item" style="background-image:url('/static/images/passion-projects-bg.jpg');">
                                <div class="project__wrap">
                                    <h3 class="heading heading--main heading--orange heading--separator-after font-size--4">
                                        Passion Projects</h3>
                                    <p class="text-align--center">Here at 502, we are always creating and working on new
                                        things internally in order to
                                        help the community, bolster our skills, or create something new and innovative.
                                        Take
                                        a sneak peak of what we are currently working on!</p>
                                    <p class="text-align--center">
                                        <a href="#" class="button">Learn More</a>
                                    </p>
                                </div>
                            </div>

                            <div class="entry grid__item" style="background-image:url('/static/images/502-podcast-bg.jpg');">
                                <div class="project__wrap project__wrap--right">
                                    <img class="alignnone" src="/static/images/502-podcast-logo.png" alt="502 Podcast"/>
                                    <p>
                                        <a href="#" class="button">Listen Now</a>
                                    </p>
                                </div>
                            </div>

                        </div>

                    </div>

                </section>

            </main><!-- #main -->

        </div><!-- #primary -->

    </div><!-- #content -->

	<?php require __DIR__ . '/template-parts/footer.php'; ?>
