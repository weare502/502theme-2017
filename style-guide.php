<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>502 Media</title>
    <link rel="stylesheet" type="text/css" href="./static/fonts/fonts.css" />
    <link rel="stylesheet" href="./static/scss/style.css">
</head>

<body class="no-js">

<script type="text/javascript">
    //<![CDATA[
    (function(){
        var c = document.body.classList;
        c.remove('no-js');
        c.add('js');
    })();
    //]]>
</script>

<div id="page" class="site">

    <a class="skip-link screen-reader-text" href="#content">Skip Links</a>

    <div class="hero">

		<?php require __DIR__ . '/template-parts/header.php'; ?>

        <header class="entry-header">

            <h1 class="entry-title" itemprop="headline">Page Title</h1>

            <p class="entry-tagline" itemprop="tagline">Tagline</p>

        </header>

    </div>

    <div id="content" class="site-content"><div id="primary" class="content-area">

        <main id="main" class="site-main">

            <article class="entry" itemscope="" itemtype="https://schema.org/CreativeWork">

                <div class="entry-content" itemprop="text">
                    <p>This is a paragraph</p>
                    <button class="button">Standard Button</button>
                    <button class="button button--dark">Dark Button</button>
                </div>

            </article>

        </main><!-- #main -->

    </div><!-- #primary --></div><!-- #content -->

	<?php require __DIR__ . '/template-parts/footer.php'; ?>

</div>

<script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script type="text/javascript" src="./static/js/site.js"></script>

</body>

</html>
