'use strict';

var gulp = require('gulp'),

    // Sass/CSS processes
    autoprefixer = require('autoprefixer'),
    cssMinify = require('gulp-cssnano'),
    mqpacker = require('css-mqpacker'),
    postcss = require('gulp-postcss'),
    sass = require('gulp-sass'),
    sassLint = require('gulp-sass-lint'),
    // sourcemaps = require('gulp-sourcemaps'),

    // Utilities
    browserSync = require('browser-sync'),
    notify = require('gulp-notify'),
    rename = require('gulp-rename'),
    plumber = require('gulp-plumber');

/*************
 * Utilities
 ************/

/**
 * Error handling
 *
 * @function
 */
function handleErrors() {
    var args = Array.prototype.slice.call(arguments);

    notify.onError({
        title: 'Task Failed [<%= error.message %>',
        message: 'See console.',
        sound: 'Sosumi' // See: https://github.com/mikaelbr/node-notifier#all-notification-options-with-their-defaults
    }).apply(this, args);

    gutil.beep(); // Beep 'sosumi' again

    // Prevent the 'watch' task from stopping
    this.emit('end');
}


/*************
 * CSS Tasks
 ************/

/**
 * PostCSS Task Handler
 */
gulp.task('postcss', function(){

    return gulp.src('static/scss/style.scss')

    // Error handling
        .pipe(plumber({
            errorHandler: handleErrors
        }))

        // Wrap tasks in a sourcemap
        // .pipe( sourcemaps.init())

        .pipe( sass({
            errLogToConsole: true,
            outputStyle: 'expanded' // Options: nested, expanded, compact, compressed
        }))

        .pipe( postcss([
            autoprefixer({
                browsers: ['last 2 versions']
            }),
            mqpacker({
                sort: true
            }),
        ]))

        // creates the sourcemap
        // .pipe(sourcemaps.write('./'))

        .pipe(gulp.dest('./static/scss/'))
        .pipe(browserSync.stream());

});

gulp.task('css:minify', ['postcss'], function() {
    return gulp.src('./static/scss/style.css')
    // Error handling
        .pipe(plumber({
            errorHandler: handleErrors
        }))

        .pipe( cssMinify({
            safe: true
        }))
        .pipe(rename('style.min.css'))
        .pipe(gulp.dest('./static/scss/'))
});

gulp.task('sass:lint', ['css:minify'], function() {
    gulp.src([
        'static/scss/style.scss',
        '!static/scss/base/html5-reset/_normalize.scss',
        '!static/scss/utilities/animate/**/*.*'
    ])
        .pipe(sassLint())
        .pipe(sassLint.format())
        .pipe(sassLint.failOnError())
});

/********************
 * All Tasks Listeners
 *******************/

gulp.task('watch', function () {
    // Kick off BrowserSync.
    browserSync.init({
        proxy: "502.dev"
    });

    gulp.watch('static/scss/**/*.scss', ['styles']);
    gulp.watch('*.php',browserSync.reload);
});

/**
 * Individual tasks.
 */
// gulp.task('scripts', [''])
gulp.task('styles', ['sass:lint'] );
gulp.task('default',['sass:lint', 'watch']);
