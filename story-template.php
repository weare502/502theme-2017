<?php
/**
 * Template Name: Story
 */

$context = Timber::get_context();
$context['post'] = Timber::get_post();
$context['hero_image'] = $context['post']->thumbnail();

$templates = array( 'story.twig' );

Timber::render( $templates, $context );