<?php require __DIR__ . '/template-parts/html-head.php'; ?>

<div id="page" class="site our-work">

    <a class="skip-link screen-reader-text" href="#content">Skip Links</a>

    <div class="hero" style="background-image: url('/static/images/our-work-header.png');">

        <div class="hero__wrap">

			<?php require __DIR__ . '/template-parts/header.php'; ?>

            <header class="entry-header entry-header--boxed">

                <h1 class="entry-title heading heading--light" itemprop="headline">Our Work</h1>

                <p class="entry-subtitle entry-subtitle--light" itemprop="tagline">See Stories in Action</p>

            </header>

        </div>

    </div>

    <div id="content" class="site-content"><div id="primary" class="content-area">

        <main id="main" class="site-main">

            <nav class="our-work__nav flex-container flex-container--wrap align-items--center" itemscope="" itemtype="https://schema.org/SiteNavigationElement" aria-label="Our Work navigation">

                <h3 class="heading heading--orange">Categories:</h3>

                <ul class="inline-list space-around" id="isotope-filters">
                    <li class="menu-item">
                        <a href="#" class="button button--small button--outline button--current-item" data-filter=".masonry-grid__grid-item">All</a>
                    </li>
                    <li class="menu-item">
                        <a href="#" class="button button--small button--outline" data-filter=".web">Web</a>
                    </li>
                    <li class="menu-item">
                        <a href="#" class="button button--small button--outline" data-filter=".video">Video</a>
                    </li>
                    <li class="menu-item">
                        <a href="#" class="button button--small button--outline" data-filter=".branding">Branding</a>
                    </li>
                </ul>

            </nav>

            <section class="our-work__brands masonry-grid" itemscope="" itemtype="https://schema.org/CreativeWork">

                <a href="/work-interior.php" class="masonry-grid__grid-item"><img src="static/images/FHFTC.jpg" alt="Brand"></a>

                <a href="/work-interior.php" class="masonry-grid__grid-item branding"><img src="static/images/Steves-Floral.jpg" alt="Brand">
                </a>

                <a href="/work-interior.php" class="masonry-grid__grid-item"><img src="static/images/DMI.jpg" alt="Brand"></a>

                <a href="/work-interior.php" class="masonry-grid__grid-item"><img src="static/images/CFA.jpg" alt="Brand"></a>

                <a href="/work-interior.php" class="masonry-grid__grid-item video"><img src="static/images/Florence.jpg" alt="Brand"></a>

                <a href="/work-interior.php" class="masonry-grid__grid-item"><img src="static/images/24_7.jpg" alt="Brand"></a>

                <a href="/work-interior.php" class="masonry-grid__grid-item"><img src="static/images/APM.jpg" alt="Brand"></a>

                <a href="/work-interior.php" class="masonry-grid__grid-item web"><img src="static/images/CW-Insurance.jpg" alt="Brand"></a>

                <a href="/work-interior.php" class="masonry-grid__grid-item"><img src="static/images/Modern-Assessor.jpg" alt="Brand"></a>

                <a href="/work-interior.php" class="masonry-grid__grid-item"><img src="static/images/Food-Integrity.jpg" alt="Brand"></a>

                <a href="/work-interior.php" class="masonry-grid__grid-item"><img src="static/images/SMH.jpg" alt="Brand"></a>

            </section>

        </main><!-- #main -->

    </div><!-- #primary --></div><!-- #content -->

	<?php require __DIR__ . '/template-parts/footer.php'; ?>
