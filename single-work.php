<?php
/**
 * Home Template File
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
 */
the_post();
$context = Timber::get_context();
$context['post'] = Timber::get_post();
$context['hero_image'] = $context['post']->thumbnail();
$templates = array( 'single-work.twig' );
weare502_modify_footer_cta( $context['post'], $context );

Timber::render( $templates, $context );