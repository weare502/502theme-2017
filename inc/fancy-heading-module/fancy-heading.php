<?php 
if ( class_exists('FLBuilder') ) :
    class WeAre502FancyHeading extends FLBuilderModule {

        public function __construct()
        {
            parent::__construct(array(
                'name'            => __( 'Fancy Heading', 'fl-builder' ),
                'description'     => __( 'A fancy heading', 'fl-builder' ),
                'category'        => __( 'Advanced Modules', 'fl-builder' ),
                'dir'             => get_stylesheet_directory() . 'inc/fancy-heading-module/',
                'url'             => get_stylesheet_directory_uri() . 'inc/fancy-heading-module/',
                'editor_export'   => true, // Defaults to true and can be omitted.
                'enabled'         => true, // Defaults to true and can be omitted.
                'partial_refresh' => false, // Defaults to false and can be omitted.
            ));
        }
    }

    FLBuilder::register_module( 'WeAre502FancyHeading', array(
        'my-tab-1'      => array(
            'title'         => __( 'Settings', 'fl-builder' ),
            'sections'      => array(
                'my-section-1'  => array(
                    'title'         => __( 'Fancy Heading', 'fl-builder' ),
                    'fields'        => array(
                        'heading_text'     => array(
                            'type'          => 'text',
                            'label'         => __( 'Heading Text', 'fl-builder' ),
                        )
                    )
                )
            )
        )
    ) );
endif;