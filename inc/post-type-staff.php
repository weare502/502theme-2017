<?php

// We're already in `init`

$labels = array(
	'name'                => __( 'Staff', 'weare502' ),
	'singular_name'       => __( 'Staff', 'weare502' ),
	'add_new'             => _x( 'Add New Staff', 'weare502', 'weare502' ),
	'add_new_item'        => __( 'Add New Staff', 'weare502' ),
	'edit_item'           => __( 'Edit Staff', 'weare502' ),
	'new_item'            => __( 'New Staff', 'weare502' ),
	'view_item'           => __( 'View Staff', 'weare502' ),
	'search_items'        => __( 'Search Staff', 'weare502' ),
	'not_found'           => __( 'No Staff found', 'weare502' ),
	'not_found_in_trash'  => __( 'No Staff found in Trash', 'weare502' ),
	'parent_item_colon'   => __( 'Parent Staff:', 'weare502' ),
	'menu_name'           => __( 'Staff', 'weare502' ),
);

$args = array(
	'labels'                   => $labels,
	'hierarchical'        => false,
	'description'         => 'Staff employed at 502',
	'taxonomies'          => array(),
	'public'              => true,
	'show_ui'             => true,
	'show_in_menu'        => true,
	'show_in_admin_bar'   => true,
	'menu_position'       => null,
	'menu_icon'           => null,
	'show_in_nav_menus'   => true,
	'publicly_queryable'  => true,
	'exclude_from_search' => false,
	'has_archive'         => false,
	'query_var'           => true,
	'can_export'          => true,
	'rewrite'             => true,
	'capability_type'     => 'post',
	'supports'            => array(
		'title', 'editor', 'thumbnail',
		'revisions', 'page-attributes'
		)
);

register_post_type( 'staff', $args );

