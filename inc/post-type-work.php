<?php

// We're already in `init`

$labels = array(
	'name'                => __( 'Work', 'weare502' ),
	'singular_name'       => __( 'Work', 'weare502' ),
	'add_new'             => _x( 'Add New Work', 'weare502', 'weare502' ),
	'add_new_item'        => __( 'Add New Work', 'weare502' ),
	'edit_item'           => __( 'Edit Work', 'weare502' ),
	'new_item'            => __( 'New Work', 'weare502' ),
	'view_item'           => __( 'View Work', 'weare502' ),
	'search_items'        => __( 'Search Work', 'weare502' ),
	'not_found'           => __( 'No Work found', 'weare502' ),
	'not_found_in_trash'  => __( 'No Work found in Trash', 'weare502' ),
	'parent_item_colon'   => __( 'Parent Work:', 'weare502' ),
	'menu_name'           => __( 'Work', 'weare502' ),
);

$args = array(
	'labels'                   => $labels,
	'hierarchical'        => false,
	'description'         => 'Work we have done for clients',
	'taxonomies'          => array(),
	'public'              => true,
	'show_ui'             => true,
	'show_in_menu'        => true,
	'show_in_admin_bar'   => true,
	'menu_position'       => null,
	'menu_icon'           => null,
	'show_in_nav_menus'   => true,
	'publicly_queryable'  => true,
	'exclude_from_search' => false,
	'has_archive'         => true,
	'query_var'           => true,
	'can_export'          => true,
	'rewrite'             => true,
	'capability_type'     => 'post',
	'supports'            => array(
		'title', 'editor', 'thumbnail',
		'revisions', 'page-attributes', 'excerpt'
		)
);

register_post_type( 'work', $args );

