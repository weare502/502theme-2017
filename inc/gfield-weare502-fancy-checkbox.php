<?php

if ( class_exists('GF_Field') ){
	class WeAre502_Field_Fancy_Checkbox extends GF_Field {
		public $type = 'weare502_fancy_checkbox';

		public function get_form_editor_field_title() {
		    return esc_attr__( 'Fancy Checkbox', 'weare502' );
		}

		public function get_form_editor_button() {
		    return array(
		        'group' => 'advanced_fields',
		        'text'  => $this->get_form_editor_field_title()
		    );
		}

		function get_form_editor_field_settings() {
			return array(
				'error_message_setting',
				'label_setting',
				'admin_label_setting',
				'description_setting',
				'css_class_setting',
				'image_url_setting'
			);
		}

		public function get_field_input( $form, $value = '', $entry = null ) {
			$form_id         = absint( $form['id'] );
			$is_entry_detail = $this->is_entry_detail();
			$is_form_editor  = $this->is_form_editor();

			$html_input_type = 'checkbox';

			$id          = (int) $this->id;
			$field_id    = $is_entry_detail || $is_form_editor || $form_id == 0 ? "input_$id" : 'input_' . $form_id . "_$id";

			$value        = esc_attr( $value );
			$size         = $this->size;
			$class_suffix = $is_entry_detail ? '_admin' : '';
			$class        = $size . $class_suffix;

			$tabindex              = $this->get_tabindex();
			$disabled_text         = $is_form_editor ? 'disabled="disabled"' : '';

			$invalid_attribute     = $this->failed_validation ? 'aria-invalid="true"' : 'aria-invalid="false"';

			if ( is_admin() ) {
				$input = '';
			} else {
				$input = "<input name='input_{$id}' id='{$field_id}' type='{$html_input_type}' value='Yes' class='{$class}' {$tabindex} {$invalid_attribute} {$disabled_text}/><label class='fancy_checkbox_button button' for='{$field_id}'><span class='fancy_checkbox_button_text'></span></label>";
			}
			
			return sprintf( "<div class='ginput_container ginput_container_fancy_checkbox'>%s</div>", $input );
		}

		public function get_field_content( $value, $force_frontend_label, $form ) {
		    $form_id         = $form['id'];
		    $admin_buttons   = $this->get_admin_buttons();
		    $is_entry_detail = $this->is_entry_detail();
		    $is_form_editor  = $this->is_form_editor();
		    $is_admin        = $is_entry_detail || $is_form_editor;
		    $image = $this->image_url;
		    $image_html = is_admin() ? "<br><img src='{$image}' width='50px' />" : "<img src='{$image}' />";
		    $field_label     = $this->get_field_label( $force_frontend_label, $value );
		    $description 	 = $this->get_description( $this->description, 'gfield_description' );
		    $field_id        = $is_admin || $form_id == 0 ? "input_{$this->id}" : 'input_' . $form_id . "_{$this->id}";
		    if ( is_admin() ) {
			    $field_content = sprintf( "%s<label class='gfield_label gfield_fancy_checkbox_label' for='%s'><div class='field_label_text_wrapper'>%s</div>%s%s<div class='fancy_checkbox_button'><span class='fancy_checkbox_button_text'></span></div></label>{FIELD}", $admin_buttons, $field_id, esc_html( $field_label ), $image_html, $description );
			} else {
				$field_content = sprintf( "<label class='gfield_label gfield_fancy_checkbox_label' for='%s'><div class='field_label_text_wrapper'>%s</div>%s%s</label>{FIELD}", $field_id, esc_html( $field_label ), $image_html, $description );
			}
		 	
		    return $field_content;
		}

		public function sanitize_entry_value( $value, $form_id ) {
		   //makes sure submitted value is an escaped string
		   $value = esc_attr( $value );
		   //return sanitized value
		   return $value;
		}

	}

	GF_Fields::register( new WeAre502_Field_Fancy_Checkbox() );

}
