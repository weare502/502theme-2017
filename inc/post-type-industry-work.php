<?php

// We're already in `init`

$labels = array(
	'name'                => __( 'Industry Work', 'weare502' ),
	'singular_name'       => __( 'Industry Work', 'weare502' ),
	'add_new'             => _x( 'Add New', 'weare502', 'weare502' ),
	'add_new_item'        => __( 'Add New Industry Work', 'weare502' ),
	'edit_item'           => __( 'Edit Industry Work', 'weare502' ),
	'new_item'            => __( 'New Industry Work', 'weare502' ),
	'view_item'           => __( 'View Industry Work', 'weare502' ),
	'search_items'        => __( 'Search Industry Work', 'weare502' ),
	'not_found'           => __( 'No Industry Work found', 'weare502' ),
	'not_found_in_trash'  => __( 'No Industry Work found in Trash', 'weare502' ),
	'parent_item_colon'   => __( 'Parent Industry Work:', 'weare502' ),
	'menu_name'           => __( 'Industry Work Landing Pages', 'weare502' ),
);

$args = array(
	'labels'              => $labels,
	'hierarchical'        => false,
	'description'         => 'Work we have done for industries',
	'taxonomies'          => array(),
	'public'              => true,
	'show_ui'             => true,
	'show_in_menu'        => true,
	'show_in_admin_bar'   => true,
	'menu_position'       => null,
	'menu_icon'           => 'dashicons-schedule',
	'show_in_nav_menus'   => true,
	'publicly_queryable'  => true,
	'exclude_from_search' => true,
	'has_archive'         => false,
	'query_var'           => true,
	'can_export'          => true,
	'rewrite'             => array('slug' => 'our-work'),
	'capability_type'     => 'post',
	'supports'            => array( 'title','thumbnail' )
);

register_post_type( 'industry_work', $args );

