<?php 
/**
 * Template Name: Contact
 */


$context = Timber::get_context();
$context['post'] = Timber::get_post();
$context['hero_image'] = $context['post']->thumbnail();
$templates = array( 'contact.twig' );
weare502_modify_footer_cta( $context['post'], $context );

Timber::render( $templates, $context );