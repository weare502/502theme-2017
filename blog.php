<?php require __DIR__ . '/template-parts/html-head.php'; ?>

<div id="page" class="site blog">

    <a class="skip-link screen-reader-text" href="#content">Skip Links</a>

    <div class="hero" style="background-image: url('/static/images/blog-bg.jpg');">

        <div class="hero__wrap">

			<?php require __DIR__ . '/template-parts/header.php'; ?>

            <header class="entry-header entry-header--boxed">

                <h1 class="entry-title heading heading--light" itemprop="headline">Authored Blog Posts</h1>

                <p class="entry-subtitle entry-subtitle--light" itemprop="tagline">See Stories in Action</p>

            </header>

        </div>

    </div>

    <div id="content" class="site-content sidebar--left clearfix">

        <div id="primary" class="content-area">

            <main id="main" class="site-main column-count--2">

                <article class="entry" itemscope="" itemtype="https://schema.org/CreativeWork">
                    <img src="/static/images/blog-featured-image-3.jpg" class="alignnone" alt="Blog image"/>
                    <header class="entry-header">
                        <h2 class="entry-title heading heading--separator-after">
                            <a href="#" class="link--inverted" rel="bookmark">What an Epic First Date Taught Me About
                                Automation</a>
                        </h2>
                    </header><!-- .entry-header -->

                    <div class="entry-content">
                        <p>My old boss once told me that in high school he had a standard first date, and it was
                            purposefully epic. Every single girl he took on a first date went on the same exact date.
                            And we’re not talking same restaurant, we are talking every single moment planned out and
                            executed in the same… <a href="#" class="more-link read-more-link">Read More</a></p>
                    </div><!-- .entry-content -->
                </article>

                <article class="entry" itemscope="" itemtype="https://schema.org/CreativeWork">
                    <img src="/static/images/blog-featured-image-4.jpg" class="alignnone" alt="Blog image"/>
                    <header class="entry-header">
                        <h2 class="entry-title heading heading--separator-after">
                            <a href="#" class="link--inverted" rel="bookmark">Is Marketing A Priority?</a>
                        </h2>
                    </header><!-- .entry-header -->

                    <div class="entry-content">
                        <p>After entering back into the for-profit world after eight years of working behind non-profit
                            doors, there is one question that sticks out to me. Why don’t many nonprofits treat
                            marketing as a priority in their budget? Yes, we all understand donors want to ensure their
                            dollars are… <a href="#" class="more-link read-more-link">Read More</a></p>
                    </div><!-- .entry-content -->
                </article>

                <article class="entry" itemscope="" itemtype="https://schema.org/CreativeWork">
                    <img src="/static/images/blog-featured-image-4.jpg" class="alignnone" alt="Blog image"/>
                    <header class="entry-header">
                        <h2 class="entry-title heading heading--separator-after">
                            <a href="#" class="link--inverted" rel="bookmark">Design Is Pointless</a>
                        </h2>
                    </header><!-- .entry-header -->

                    <div class="entry-content">
                        <p>Wait, did the Art Director just say that design is pointless? Yes. Yes, he did. But it comes
                            with one big asterisk. Let me explain. I was one of those lucky kids who stumbled into a
                            college major that he loved and never looked back. Design was this incredible thing where I
                            could make things… <a href="#" class="more-link read-more-link">Read More</a></p>
                    </div><!-- .entry-content -->
                </article>

                <article class="entry" itemscope="" itemtype="https://schema.org/CreativeWork">
                    <img src="/static/images/blog-featured-image-3.jpg" class="alignnone" alt="Blog image"/>
                    <header class="entry-header">
                        <h2 class="entry-title heading heading--separator-after">
                            <a href="#" class="link--inverted" rel="bookmark">A Little Content Goes A Long Way</a>
                        </h2>
                    </header><!-- .entry-header -->

                    <div class="entry-content">
                        <p>Facebook for business is great. It is super easy to connect with customers now more than ever
                            before. But one thing that sometimes gets forgotten in between the “Come see our new
                            products!” and “Widgets now on sale!” is just plain old-fashioned content. Sure, your
                            followers… <a href="#" class="more-link read-more-link">Read More</a></p>
                    </div><!-- .entry-content -->
                </article>

            </main><!-- #main -->

            <div class="archive-pagination pagination text-align--center">
                <a class="button button--outline" href="#">View All Posts</a>
            </div>

        </div><!-- #primary -->

        <aside id="secondary" class="widget-area">

            <section class="widget">
                <h3 class="heading font-size--4">Categories</h3>
                <ul>
                    <li>
                        <a href="#" class="link--inverted current-item">All</a>
                    </li>
                    <li>
                        <a href="#" class="link--inverted">Marketing</a>
                    </li>
                    <li>
                        <a href="#" class="link--inverted">Branding</a>
                    </li>
                    <li>
                        <a href="#" class="link--inverted">Design</a>
                    </li>
                    <li>
                        <a href="#" class="link--inverted">Account Management</a>
                    </li>
                </ul>
            </section>
            <section class="widget">
                <h3 class="heading font-size--4">Author</h3>
                <ul>
                    <li>
                        <a href="#" class="link--inverted">Jordan</a>
                    </li>
                    <li>
                        <a href="#" class="link--inverted">Blade</a>
                    </li>
                    <li>
                        <a href="#" class="link--inverted">Kehoulani</a>
                    </li>
                    <li>
                        <a href="#" class="link--inverted">Kaila</a>
                    </li>
                    <li>
                        <a href="#" class="link--inverted">Collin</a>
                    </li>
                    <li>
                        <a href="#" class="link--inverted">Daron</a>
                    </li>
                    <li>
                        <a href="#" class="link--inverted">Leah</a>
                    </li>
                    <li>
                        <a href="#" class="link--inverted">Darcie</a>
                    </li>
                    <li>
                        <a href="#" class="link--inverted">Sam</a>
                    </li>
                    <li>
                        <a href="#" class="link--inverted">Christy</a>
                    </li>
                </ul>
            </section>
        </aside><!-- #secondary -->

    </div><!-- #content -->

	<?php require __DIR__ . '/template-parts/footer.php'; ?>
