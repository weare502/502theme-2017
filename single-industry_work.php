<?php
/**
 * Template Name: Work
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
 */

$context = Timber::get_context();
$context['post'] = Timber::get_post();
$context['hero_image'] = $context['post']->thumbnail();
$context['work'] = Timber::get_posts( $context['post']->get_field('industry_work') );
$templates = array( 'work.twig' );
weare502_modify_footer_cta( $context['post'], $context );

Timber::render( $templates, $context );