<?php require __DIR__ . '/template-parts/html-head.php'; ?>

<div id="page" class="site our-story">

    <a class="skip-link screen-reader-text" href="#content">Skip Links</a>

    <div class="hero" style="background-image: url('/static/images/Blade.jpg');">

        <div class="hero__wrap">

			<?php require __DIR__ . '/template-parts/header.php'; ?>

            <header class="entry-header">

                <h1 class="entry-title heading heading--light" itemprop="headline">The 502</h1>

                <p class="entry-subtitle entry-subtitle--light" itemprop="subtitle">The history of 502 is a story of
                    pluck that exemplifies the spirit of entrepreneurism</p>

            </header>

            <a href="#main" class="hero__scroll-indicator scroll-to">
                <span class="scroll-indicator__text">Scroll</span>
                <span class="scroll-indicator__arrow"></span>
            </a>

        </div>

    </div>

    <div id="content" class="site-content"><div id="primary" class="content-area">

        <main id="main" class="site-main">

            <article class="entry" itemscope="" itemtype="https://schema.org/CreativeWork">

                <div class="entry-content" itemprop="text">
                    <p><span class="first-letter">B</span>lade Mages didn’t set out to create a production company, but
                        he wasn’t going to let a decisive opportunity pass him by. At 19, he was working at the
                        Manhattan Regional Airport and frustrated because his plan for the future had been upended
                        again. Recently receiving his pilot’s license with plans to be a commercial pilot, his dreams
                        were dashed when he flew with a seasoned pilot who spoke with nostalgia of the smaller aircraft.
                        Realizing that piloting a commercial plane was nothing like flying the small planes he loved, he
                        knew he would never be happy in that career.</p>

                    <img class="alignright" src="static/images/story-illustration-1.png" alt="Phone illustration" />

                    <p>Blade was in the midst of a heated internal debate about his future when he received an odd phone
                        call. The weathered voice of his high school video instructor, John Culley, spoke with urgency
                        on the other end of the line. In school, Blade had fallen in love with video production. The
                        news-style show he produced with his friends was featured on the local cable television station,
                        the owner of which, Culley was urging him to contact immediately. How weird, Blade thought; but
                        he complied, quickly dialing the number of the station. The owner directed him to a man on the
                        East Coast who needed a contractor to produce short commercials for small town businesses to
                        play on local cable stations.</p>

                    <p><span class="inline-quote">“I was told you own a production company,”</span> the man said.
                        <span class="inline-quote">“Would you be interested?”</span></p>

                    <p>Recognizing the potential of the offer, Blade seized the opportunity to forge a new path for his
                        future.</p>

                    <p><span class="inline-quote">“Okay, sure!”</span> he responded. <span class="inline-quote">“I could handle that.”</span>
                    </p>

                    <p><span class="inline-quote">“What kind of equipment do you shoot on?”</span> the man asked.</p>

                    <p>At this time, the only piece of technology Blade owned was in his hand: a Blackberry Curve.
                        Thankfully, he had been drooling over new video gear for months. He quickly rattled off his wish
                        list of equipment.</p>

                    <p>Satisfied, the man told him he’d come to town in two weeks to view Blade’s portfolio and conduct
                        an official interview.</p>

                    <img class="alignleft" src="static/images/story-illustration-2.png" alt="Portfolio illustration" />

                    <p><span class="inline-quote">“Oh, by the way, what’s the name of your production company?”</span>
                        he asked.</p>

                    <p>Blade drew a blank. His production company was only moments old.</p>

                    <p>Scrambling for inspiration, Blade glanced at his desk and noticed an envelope that was halfway
                        covered by a sheet of paper. Only the 502 in the 66502 of Manhattan’s zip code was showing.
                        <span class="inline-quote">“502 Productions?”</span> he ventured, unsure.</p>

                    <p><span class="inline-quote">“Sounds good.”</span> The man said and hung up.
                    </p>

                    <p>Luckily for Blade, this was before the instant gratification of internet video. He had exactly
                        two weeks to secure a loan, purchase the equipment he had promised, and produce a solid
                        commercial to show the man. In the end, he produced three.</p>

                    <p>That year, Blade’s little red Ford Escort with 197,000 miles drove all over Kansas, meeting with
                        clients and producing 30 second T.V. commercials.</p>

                    <p>Eventually, Blade began taking on larger productions, regional clients, and employees. It wasn’t
                        long before 502 was a bona fide agency, handling all kinds of media, not just video. More than
                        the deliverable, clients were drawn to 502 for their innovative attitude.</p>

                    <p>502 was built on pluck, courage, and bold, decisive moves. It prides itself on its creativity,
                        its early adoption of cutting edge techniques, and its willingness to think outside of the box.
                        Maturing into an agency full of experienced creatives, 502 emboldens their clients to share
                        their authentic stories to connect with their customers on a deeper level.</p>

                    <img class="aligncenter" src="static/images/story-illustration-3.png" alt="Production illustration" />

                    <p><span class="first-letter">A</span>s an agency, we don’t believe in safe or status quo. We don’t
                        believe in going with the flow. We are constantly learning, adapting, and pushing ourselves and
                        our clients higher than we ever thought possible. </p>

                    <hr>

                    <h2 class="heading call-out color--orange">Ditch the never-ending busy work and focus on work that matters. Let’s create something bigger
                        than ourselves, together.</h2>

                </div>

            </article>

        </main><!-- #main -->

    </div><!-- #primary --></div><!-- #content -->

	<?php require __DIR__ . '/template-parts/footer.php'; ?>
