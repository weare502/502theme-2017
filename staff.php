<?php require __DIR__ . '/template-parts/html-head.php'; ?>

<div id="page" class="site full-width">

    <a class="skip-link screen-reader-text" href="#content">Skip Links</a>

    <div class="hero" style="background-image: url('/static/images/Hero.jpg');">

        <div class="hero__wrap">

			<?php require __DIR__ . '/template-parts/header.php'; ?>

            <header class="entry-header entry-header--boxed">

                <h1 class="entry-title heading heading--light" itemprop="headline">About Us</h1>

                <p class="entry-subtitle entry-subtitle--light" itemprop="tagline">Learn Who We Are</p>

            </header>

        </div>

    </div>

    <div id="content" class="site-content"><div id="primary" class="content-area">

        <main id="main" class="site-main">

            <section class="section--full-width" itemscope="" itemtype="https://schema.org/CreativeWork">

                <div class="section__wrap section__wrap--wide">

                    <h2 class="heading heading--main heading--separator-after">The 502 Approach</h2>

                    <div class="width--70 margin-bottom--6">

                        <p>If you're reading this page, and this isn't your first rodeo, you're probably expecting the
                            obligatory Proprietary Process® where we wow you with a series of buzzwords
                            like...Synthesize...Are you impressed?</p>

                        <p>Truth be told, we work a lot like you work. We see a problem. We tackle the problem. We
                            repeat. No jargon or proprietary formula needed. When looking for an agency, you need an
                            approach that is real and relatable. Something human.</p>

                        <h3 class="heading heading--orange font-size--2">All that aside, you need to know what you're
                            getting into. At 502, our approach is simple:</h3>
                    </div>

                    <div class="grid grid--3 grid-gutter--30">
                        <div class="grid__item grid__item--shadow grid__item--card">
                            <div class="flex-container align-items--center fixed-height--100">
                                <img class="aligncenter" src="/static/images/we-listen-graphic.png" alt="We listen"/>
                            </div>
                            <h4 class="heading heading--main heading--separator-after font-size--4">We Listen</h4>
                            <p>Our industry is famous for skipping this step. You have ideas. You know your business.
                                You know your story. So, that's where we start. We will never claim to know more about
                                your organization than you will. We start every relationship with open ears.
                            </p>
                            <p class="inline-quote text-align--center">We listen for the stories that aren't being
                                told.</p>
                        </div>

                        <div class="grid__item grid__item--shadow grid__item--card">
                            <div class="flex-container align-items--center fixed-height--100">
                                <img class="aligncenter" src="/static/images/we-create-graphic.png" alt="We listen"/>
                            </div>
                            <h4 class="heading heading--main heading--separator-after font-size--4">We Create</h4>
                            <p>Using a narrative-driven approach, we work to tell the story of your company and how you
                                can help your customers. But, telling a great story isn't enough. Results are what
                                matter. </p> 
                            <p>Every idea, strategy, theme, plan and message we produce, regardless of the channel it's
                                delivered through, is tied to the bottom line: moving the needle in areas that matter to
                                your business.
                            </p>
                            <p class="inline-quote text-align--center">We create the tools to tell your story & get
                                results.</p>
                        </div>

                        <div class="grid__item grid__item--shadow grid__item--card">
                            <div class="flex-container align-items--center fixed-height--100">
                                <img class="aligncenter" src="/static/images/we-execute-graphic.png" alt="We listen"/>
                            </div>
                            <h4 class="heading heading--main heading--separator-after font-size--4">We Execute</h4>
                            <p>With a staff of in-house creatives, we take your story and the strategy behind it to
                                market. We breathe life into your message through beautiful works of design and copy,
                                motion and code, film and experience.</p>
                            <p class="inline-quote text-align--center">We take your story to the world.</p>
                        </div>
                    </div>
                </div>

            </section>

            <section class="section--full-width" itemscope="" itemtype="https://schema.org/CreativeWork">

                <div class="section__wrap section__wrap--wide">

                    <h2 class="heading heading--main heading--separator-after">The Staff</h2>

                    <div class="grid grid--3 staff-profiles grid-gutter--30">
                        <a href="/staff-interior.php" class="grid__item staff-profile clearfix">
                            <img src="/static/images/staff-profile-blade.jpg" alt="Blade">
                            <div class="staff-profile__info">
                                <span class="staff-profile__name">Blade</span>
                                <span class="staff-profile__role">Principal / Founder</span>
                            </div>
                        </a>

                        <a href="/staff-interior.php" class="grid__item staff-profile clearfix">
                            <img src="/static/images/staff-profile-kehoulani.jpg" alt="Kehoulani">
                            <div class="staff-profile__info">
                                <span class="staff-profile__name">Kehoulani</span>
                                <span class="staff-profile__role">Creative Director</span>
                            </div>
                        </a>

                        <a href="/staff-interior.php" class="grid__item staff-profile clearfix">
                            <img src="/static/images/staff-profile-christy.jpg" alt="Christy">
                            <div class="staff-profile__info">
                                <span class="staff-profile__name">Christy</span>
                                <span class="staff-profile__role">Production Manager</span>
                            </div>
                        </a>

                        <a href="/staff-interior.php" class="grid__item staff-profile clearfix">
                            <img src="/static/images/staff-profile-collin.jpg" alt="Collin">
                            <div class="staff-profile__info">
                                <span class="staff-profile__name">Collin</span>
                                <span class="staff-profile__role">User Experience Designer</span>
                            </div>
                        </a>

                        <a href="/staff-interior.php" class="grid__item staff-profile clearfix">
                            <img src="/static/images/staff-profile-jordan.jpg" alt="Jordan">
                            <div class="staff-profile__info">
                                <span class="staff-profile__name">Jordan</span>
                                <span class="staff-profile__role">Art Director</span>
                            </div>
                        </a>

                        <a href="/staff-interior.php" class="grid__item staff-profile clearfix">
                            <img src="/static/images/staff-profile-kaila.jpg" alt="Kaila">
                            <div class="staff-profile__info">
                                <span class="staff-profile__name">Kaila</span>
                                <span class="staff-profile__role">Content Strategist</span>
                            </div>
                        </a>

                        <a href="/staff-interior.php" class="grid__item staff-profile clearfix">
                            <img src="/static/images/staff-profile-leah.jpg" alt="Leah">
                            <div class="staff-profile__info">
                                <span class="staff-profile__name">Leah</span>
                                <span class="staff-profile__role">Account Manager</span>
                            </div>
                        </a>

                        <a href="/staff-interior.php" class="grid__item staff-profile clearfix">
                            <img src="/static/images/staff-profile-sam.jpg" alt="Sam">
                            <div class="staff-profile__info">
                                <span class="staff-profile__name">Sam</span>
                                <span class="staff-profile__role">Account Coordinator</span>
                            </div>
                        </a>

                        <a href="/staff-interior.php" class="grid__item staff-profile clearfix">
                            <img src="/static/images/staff-profile-daron.jpg" alt="Daron">
                            <div class="staff-profile__info">
                                <span class="staff-profile__name">Daron</span>
                                <span class="staff-profile__role">Web Developer</span>
                            </div>
                        </a>
                    </div>

                </div>

            </section>

            <section class="section--full-width section--background-image background-image--box-shadow" style="background-image:url('/static/images/work-interior-background.png');" itemscope="" itemtype="https://schema.org/CreativeWork">

                <div class="section__wrap section__wrap--wide">

                    <h2 class="heading heading--main heading--separator-after">Our Space</h2>

                    <p class="width--70">We proudly call Downtown Manhattan our home. Our newly renovated downtown suite
                        sits in
                        the middle of a growing and thriving district. Stop by anytime and chat with us. We never charge
                        for meetings and are always willing to talk about your story.</p>

                    <div class="grid grid--4 grid-gutter--30 grid__our-space">
                        <img class="grid__item our-space our-space--large" src="/static/images/our-space-1.jpg" alt="Our office space"/>
                        <img class="grid__item our-space" src="/static/images/our-space-2.jpg" alt="Our office space"/>
                        <img class="grid__item our-space" src="/static/images/our-space-3.jpg" alt="Our office space"/>
                        <img class="grid__item our-space" src="/static/images/our-space-4.jpg" alt="Our office space"/>
                        <img class="grid__item our-space" src="/static/images/our-space-5.jpg" alt="Our office space"/>
                    </div>

                </div>

            </section>

            <section class="section--full-width" itemscope="" itemtype="https://schema.org/CreativeWork">

                <div class="section__wrap section__wrap--wide">

                    <h2 class="heading heading--main heading--separator-after">Our Culture</h2>

                    <p class="width--65 text-align--center">Our model was built around Midwest businesses’ marketing
                        budgets, people, and needs. Everything from video  production to content strategy is done by our
                        staff. We are partners with our clients and believe that our success and growth is a direct
                        reflection of theirs.</p>

                    <div class="grid grid--2 grid-gutter--30 grid--post-links">

                        <a class="entry grid__item post-link--bg-image" href="/blog.php" style="background-image:url('/static/images/blog-featured-image-1.jpg');">
                            <h3 class="heading heading--light heading--separator-after--white post-title font-size--4">
                                Why Being Distracted At
                                Work Can Help Productivity</h3>
                            <span class="post-term font-size--2">Work Culture</span>
                        </a>

                        <a class="entry grid__item post-link--bg-image" href="/blog.php" style="background-image:url('/static/images/blog-featured-image-2.jpg');">
                            <h3 class="heading heading--light heading--separator-after--white post-title font-size--4">
                                You. Are. Creative.</h3>
                            <span class="post-term font-size--2">Marketing</span>
                        </a>

                    </div>

                    <p class="text-align--center">
                        <a class="button button--outline" href="/blog.php">View All Posts</a>
                    </p>

                </div>

            </section>

            <section class="section--full-width section--testimonials-slider section--background-image background-image--box-shadow" style="background-image:url('/static/images/work-interior-background.png');" itemscope="" itemtype="https://schema.org/CreativeWork">

                <h2 class="heading heading--main heading--separator-after">What Our Clients Say</h2>

                <div class="testimonials-slider" style="background-image:url('/static/images/testimonials-bg.jpg');">
                    <div class="testimonials-slider__slide">
                        <div class="testimonials-slider__testimonial-quote">
                            <h3 class="heading heading--separator-after">“Their work was exceptional and their attention
                                to detail
                                made every phase of the project seamless. They are trustworthy and capable. Whatever the
                                need, I
                                believe they have the talent and capacity to  deliver results.”</h3>
                            <span class="quote-credits color--orange">Doug Hofbauer, Frontier Farm Credit</span>
                        </div>
                    </div>
                    <div class="testimonials-slider__slide">
                        <div class="testimonials-slider__testimonial-quote">
                            <h3 class="heading heading--separator-after">“Their work was exceptional and their attention
                                to detail
                                made every phase of the project seamless. They are trustworthy and capable. Whatever the
                                need, I
                                believe they have the talent and capacity to  deliver results.”</h3>
                            <span class="quote-credits color--orange">Doug Hofbauer, Frontier Farm Credit</span>
                        </div>
                    </div>
                </div>

            </section>

            <section class="section--full-width" itemscope="" itemtype="https://schema.org/CreativeWork">

                <div class="section__wrap section__wrap--wide">

                    <h2 class="heading heading--main heading--separator-after">Career Openings</h2>

                    <p class="width--65">Here at 502, we are always growing and looking for talented,
                        dedicated, and value-driven individuals to join our team. We provide awesome
                        <span class="color--orange">benefits</span> and
                        competitive pay. If You are Interested, please apply below: We would love to talk with you.</p>

                    <div class="grid grid--2 grid-gutter--30 grid--post-links">

                        <div class="entry grid__item">
                            <img class="alignnone" src="/static/images/summer-internship-web-dev.jpg" alt="Summer internship"/>
                            <h3 class="heading heading--orange heading--separator-after post-title font-size--4">
                                Summer Internship: Junior
                                Web Developer</h3>
                            <p>We are hiring a Junior Web Developer for the 2018 Summer. This position will work
                                part-time with our creative team. You will be responsible for creating and executing
                                graphic elements for our clients.</p>
                            <p>
                                <a href="#" class="more-link read-more-link font-size--2">Learn More</a>
                            </p>
                        </div>

                        <div class="entry grid__item">
                            <img class="alignnone" src="/static/images/careers-account-coordinator.jpg" alt="Summer internship"/>
                            <h3 class="heading heading--orange heading--separator-after post-title font-size--4">
                                Account Coordinator</h3>
                            <p>We are hiring an Account Coordinator. This position will work full time with our account
                                and creative team. You will be respoinsible for managing and maintaining communication,
                                projects, and deliverables for our great clients. 2 years of project management
                                experience is a plus, but not required. Hours are 8 am - 5 pm on Monday
                                through Friday. </p>
                            <p>
                                <a href="#" class="more-link read-more-link font-size--2">Learn More</a>
                            </p>
                        </div>

                    </div>

                    <p class="text-align--center">
                        <a class="button button--outline" href="/blog.php">View All Posts</a>
                    </p>

                </div>

            </section>

        </main><!-- #main -->

    </div><!-- #primary --></div><!-- #content -->

	<?php require __DIR__ . '/template-parts/footer.php'; ?>
