<?php
/**
 * Home Template File
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
 */

$context = Timber::get_context();
$context['post'] = Timber::get_post();
$context['hero_image'] = $context['post']->thumbnail();
$templates = array( 'single-staff.twig' );
$user = $context['post']->get_field('blog_user');
$context['authored_posts'] = Timber::get_posts( array( 'author' => $user['ID'], 'post_type' => 'post', 'posts_per_page' => 2 ) );

weare502_modify_footer_cta( $context['post'], $context );

Timber::render( $templates, $context );