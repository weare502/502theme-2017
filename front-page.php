<?php
/**
 * Home Template File
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
 */

$context = Timber::get_context();
$context['post'] = Timber::get_post();
$context['hero_wrap_class'] = 'flex-container flex-column';
$templates = array( 'front-page.twig' );

Timber::render( $templates, $context );