<?php require __DIR__ . '/template-parts/html-head.php'; ?>

<div id="page" class="site">

    <a class="skip-link screen-reader-text" href="#content">Skip Links</a>

    <div class="hero" style="background-image: url('/static/images/contact-us-bg.jpg');">

        <div class="hero__wrap">

			<?php require __DIR__ . '/template-parts/header.php'; ?>

            <header class="entry-header entry-header--boxed">

                <h1 class="entry-title heading heading--light" itemprop="headline">Contact Us</h1>

                <p class="entry-subtitle entry-subtitle--light" itemprop="tagline">We Would Love to Hear from You</p>

            </header>

        </div>

    </div>

    <div id="content" class="site-content max-width--960"><div id="primary" class="content-area">

        <main id="main" class="site-main">

            <article class="entry" itemscope="" itemtype="https://schema.org/CreativeWork">

                <div class="entry-content" itemprop="text">

                    <div class="gf_browser_gecko gform_wrapper contact-form_wrapper" id="gform_wrapper_11">
                        <form method="post" enctype="multipart/form-data" id="gform_11" class="contact-form" action="/image-upload/">
                            <div class="gform_body">
                                <ul id="gform_fields_11" class="gform_fields top_label form_sublabel_below description_below">
                                    <li id="field_11_1" class="gfield gfield_html gfield_html_formatted gfield_no_follows_desc field_sublabel_below field_description_below gfield_visibility_visible">
                                        <h2 class="heading heading--main heading--separator-after">Introduce
                                            Yourself</h2></li>
                                    <li id="field_11_2" class="gfield two-thirds field_sublabel_hidden_label field_description_below hidden_label gfield_visibility_visible">
                                        <label class="gfield_label gfield_label_before_complex" for="input_11_2_3">Name</label>
                                        <div class="ginput_complex ginput_container no_prefix has_first_name no_middle_name has_last_name no_suffix gf_name_has_2 ginput_container_name gfield_trigger_change" id="input_11_2">

                            <span id="input_11_2_3_container" class="name_first">
                                                    <input name="input_2.3" id="input_11_2_3" value="" aria-label="First name" tabindex="2" aria-invalid="false" placeholder="First Name" type="text">
                                                    <label for="input_11_2_3" class="hidden_sub_label screen-reader-text">First</label>
                                                </span>

                                            <span id="input_11_2_6_container" class="name_last">
                                                    <input name="input_2.6" id="input_11_2_6" value="" aria-label="Last name" tabindex="4" aria-invalid="false" placeholder="Last Name" type="text">
                                                    <label for="input_11_2_6" class="hidden_sub_label screen-reader-text">Last</label>
                                                </span>

                                        </div>
                                    </li>
                                    <li id="field_11_3" class="gfield one-third field_sublabel_below field_description_below hidden_label gfield_visibility_visible">
                                        <label class="gfield_label" for="input_11_3">Company</label>
                                        <div class="ginput_container ginput_container_text">
                                            <input name="input_3" id="input_11_3" value="" class="large" tabindex="6" placeholder="Company" aria-invalid="false" type="text">
                                        </div>
                                    </li>
                                    <li id="field_11_4" class="gfield one-half field_sublabel_below field_description_below hidden_label gfield_visibility_visible">
                                        <label class="gfield_label" for="input_11_4">Email</label>
                                        <div class="ginput_container ginput_container_email">
                                            <input name="input_4" id="input_11_4" value="" class="large" tabindex="7" placeholder="Email Address" aria-invalid="false" type="text">
                                        </div>
                                    </li>
                                    <li id="field_11_5" class="gfield one-half field_sublabel_below field_description_below hidden_label gfield_visibility_visible">
                                        <label class="gfield_label" for="input_11_5">Phone</label>
                                        <div class="ginput_container ginput_container_phone">
                                            <input name="input_5" id="input_11_5" value="" class="large" tabindex="8" placeholder="Phone Number" aria-invalid="false" type="text">
                                        </div>
                                    </li>
                                    <li id="field_11_6" class="gfield one-half field_sublabel_below field_description_below hidden_label gfield_visibility_visible">
                                        <label class="gfield_label">Misc</label>
                                        <div class="ginput_container ginput_container_checkbox">
                                            <ul class="gfield_checkbox" id="input_11_6">
                                                <li class="gchoice_11_6_1">
                                                    <input name="input_6.1" value="I am contacting 502 in regards to employment or shadow day" id="choice_11_6_1" tabindex="9" type="checkbox">
                                                    <label for="choice_11_6_1" id="label_11_6_1">I am contacting 502 in
                                                        regards to employment or shadow day</label>
                                                </li>
                                            </ul>
                                        </div>
                                    </li>
                                    <li id="field_11_7" class="gfield gfield_html gfield_html_formatted gfield_no_follows_desc field_sublabel_below field_description_below gfield_visibility_visible">
                                        <h2 class="heading heading--main heading--separator-after">How Can We Help Tell
                                            Your Story?</h2></li>
                                    <li id="field_11_9" class="gfield field_sublabel_below field_description_below hidden_label gfield_visibility_visible">
                                        <label class="gfield_label" for="input_11_9">What problem are you trying to
                                            solve?</label>
                                        <div class="ginput_container ginput_container_textarea">
                                            <textarea name="input_9" id="input_11_9" class="textarea large" tabindex="10" placeholder="What Problem Are You Trying to Solve?" aria-invalid="false" rows="10" cols="50"></textarea>
                                        </div>
                                    </li>
                                    <li id="field_11_10" class="gfield field_sublabel_below field_description_below hidden_label gfield_visibility_visible">
                                        <label class="gfield_label" for="input_11_10">What Is Your Timeline &amp;
                                            Budget?</label>
                                        <div class="ginput_container ginput_container_textarea">
                                            <textarea name="input_10" id="input_11_10" class="textarea large" tabindex="11" placeholder="What Is Your Timeline &amp; Budget?" aria-invalid="false" rows="10" cols="50"></textarea>
                                        </div>
                                    </li>
                                    <li id="field_11_11" class="gfield field_sublabel_below field_description_below hidden_label gfield_visibility_visible">
                                        <label class="gfield_label" for="input_11_11">Tell Us About Your Story So
                                            Far</label>
                                        <div class="ginput_container ginput_container_textarea">
                                            <textarea name="input_11" id="input_11_11" class="textarea large" tabindex="12" placeholder="Tell Us About Your Story So Far" aria-invalid="false" rows="10" cols="50"></textarea>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="gform_footer top_label">
                                <input id="gform_submit_button_11" class="gform_button button" value="Send" tabindex="13" onclick="if(window[&quot;gf_submitting_11&quot;]){return false;}  window[&quot;gf_submitting_11&quot;]=true;  " onkeypress="if( event.keyCode == 13 ){ if(window[&quot;gf_submitting_11&quot;]){return false;} window[&quot;gf_submitting_11&quot;]=true;  jQuery(&quot;#gform_11&quot;).trigger(&quot;submit&quot;,[true]); }" type="submit">
                                <input class="gform_hidden" name="is_submit_11" value="1" type="hidden">
                                <input class="gform_hidden" name="gform_submit" value="11" type="hidden">

                                <input class="gform_hidden" name="gform_unique_id" value="" type="hidden">
                                <input class="gform_hidden" name="state_11" value="WyJbXSIsIjdmZDFlNWIyNTBiMWMzMzBjYzk5ZGMzNTZjYzljMjU4Il0=" type="hidden">
                                <input class="gform_hidden" name="gform_target_page_number_11" id="gform_target_page_number_11" value="0" type="hidden">
                                <input class="gform_hidden" name="gform_source_page_number_11" id="gform_source_page_number_11" value="1" type="hidden">
                                <input name="gform_field_values" value="" type="hidden">

                            </div>
                        </form>
                    </div>

                </div>

            </article>

        </main><!-- #main -->

    </div><!-- #primary --></div><!-- #content -->

<!--	--><?php //require __DIR__ . '/template-parts/footer.php'; ?>
</div><!-- #page -->

<script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script src="https://unpkg.com/isotope-layout@3/dist/isotope.pkgd.min.js"></script>
<script type="text/javascript" src="./static/js/site.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.7.1/slick.min.js"></script>

</body>

</html>
