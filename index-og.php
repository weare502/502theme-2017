<html lang="en">
<head>
    <!-- START NO INDEX -->
    <meta name="robots" content="noindex">
    <!-- END NO INDEX -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>502 Media</title>
    <script src="https://use.typekit.net/pkg3bkv.js"></script>
    <script>try{Typekit.load({ async: true });}catch(e){}</script>
    <link rel="stylesheet" type="text/css" href="https://cloud.typography.com/7203196/6234572/css/fonts.css" />
    <!--    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">-->
    <!-- START IMPORT PLUGIN STYLES -->
    <link rel="stylesheet" href="../plugins/gravityforms/formsmain.css">
    <!-- END IMPORT PLUGIN STYLES -->
    <link rel="stylesheet" href="../static/scss/style.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.7.1/slick.min.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.7.1/slick-theme.min.css"/>
</head>

<!-- The home page needs a body--full-width class -->
<body class="no-js front-page body--full-width">

<script type="text/javascript">
    //<![CDATA[
    (function(){
        var c = document.body.classList;
        c.remove('no-js');
        c.add('js');
    })();
    //]]>
</script>

<?php require __DIR__ . '/template-parts/menu.php'; ?>

<div id="page" class="site">

    <a class="skip-link screen-reader-text" href="#content">Skip Links</a>

    <div class="hero">

        <div class="hero__wrap flex-container flex-column">

			<?php require __DIR__ . '/template-parts/header.php'; ?>

            <nav class="front-page__nav flex-container">

                <ul id="front-page-menu" class="menu nav-menu inline-flex-container flex-container--wrap">
                    <li class="menu-item">
                        <a href="/staff.php" data-hover="About Us"><span>We</span></a></li>
                    <li class="menu-item">
                        <a href="/work.php" data-hover="Clients"><span>Grow</span></a></li>
                    <li class="menu-item">
                        <a href="#" data-hover="Something"><span>Midwest Brands</span></a></li>
                    <li class="menu-item">
                        <a href="/values.php" data-hover="Values"><span>Through</span></a></li>
                    <li class="menu-item">
                        <a href="/story.php" data-hover="Our Story"><span>The Power of Story</span></a></li>
                </ul>

            </nav>

        </div>

    </div>

<!--	--><?php //require __DIR__ . '/template-parts/footer.php'; ?>
</div><!-- #page -->

<script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script src="https://unpkg.com/isotope-layout@3/dist/isotope.pkgd.min.js"></script>
<script type="text/javascript" src="./static/js/site.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.7.1/slick.min.js"></script>

</body>

</html>
