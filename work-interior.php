<?php require __DIR__ . '/template-parts/html-head.php'; ?>

<div id="page" class="site full-width">

    <a class="skip-link screen-reader-text" href="#content">Skip Links</a>

    <div class="hero" style="background-image: url('/static/images/cw-insurance-header.jpg');">

        <div class="hero__wrap">

			<?php require __DIR__ . '/template-parts/header.php'; ?>

            <header class="entry-header entry-header--boxed">

                <h1 class="entry-title heading heading--light" itemprop="headline">C&W Insurance</h1>

            </header>

        </div>

    </div>

    <div id="content" class="site-content"><div id="primary" class="content-area">

        <main id="main" class="site-main">

            <section class="section--full-width" itemscope="" itemtype="https://schema.org/CreativeWork">

                <div class="section__wrap">

                    <h2 class="heading heading--main heading--separator-after">Rejuvenating a wealth of history in a
                        modern era</h2>

                    <p class="text-align--center width--65">Charlson & Wilson approached 502 with the desire to
                        update and upgrade
                        their current web
                        presence.  The out-dated appearance of their site didn’t reflect who they are and the service
                        they provide.</p>

                    <p>
                        <img class="aligncenter" src="static/images/cw-insurance-video-block.jpg" alt="Video"/>
                    </p>

                </div>

            </section>

            <section class="section--full-width" itemscope="" itemtype="https://schema.org/CreativeWork">

                <div class="section__wrap">

                    <div class="flex-container flex-container--wrap">

                        <p class="flex-item flex-item--two-thirds">
                            <img class="alignnone" src="static/images/cw-insurance-website-preview.jpg" alt="Website preview"/>
                        </p>

                        <div class="flex-item flex-item--one-third">

                            <h2 class="heading heading--main">C&W Website</h2>

                            <p>After digging into the project in partnership, it became clear that an updated website
                                wouldn’t address the larger issue. How Charlson & Wilson was branded and perceived in
                                the community was the challenge that needed to be overcome.</p>

                            <p>By diving deep into the history of Charlson & Wilson, who they’ve been and who they want
                                to be, we were able to make recommendations on branding, website presence, and future
                                marketing techniques.</p>

                        </div>
                    </div>

                </div>

            </section>

            <section class="section--full-width section--background-image background-image--box-shadow" itemscope="" itemtype="https://schema.org/CreativeWork" style="background-image:url('/static/images/work-interior-background.png');">

                <div class="section__wrap">

                    <p>
                        <img class="aligncenter" src="/static/images/cw-screenshots.png" alt="CW Insurance screenshots"/>
                    </p>

                    <h3 class="heading call-out color--orange width--two-thirds">C+W wanted a modern website that was
                        easy for their
                        customers to report a claim whenever & wherever.</h3>


                </div>

            </section>

            <section class="section--full-width" itemscope="" itemtype="https://schema.org/CreativeWork">

                <div class="section__wrap">

                    <div class="flex-container flex-container--wrap">

                        <div class="flex-item flex-item--one-half">

                            <h2 class="heading heading--main">Re-Branding</h2>

                            <p>So, through research and discovery, 502 worked alongside Charlson & Wilson to rebrand the
                                company to C&W. It was important to showcase the rich history of the founders who made
                                Charlson & Wilson what it is today, but also appeal to a variety of demographics.</p>

                            <p>By stepping back to look at the larger issue, C&W was able to strengthen their position
                                as they move into a new era and connect with the next generation of Manhattanites. And,
                                once that was solved, their website and other communication pieces were able to be
                                designed efficiently and effectively.</p>

                        </div>

                        <p class="flex-item flex-item--one-half">
                            <img class="alignnone" src="static/images/cw-logo.png" alt="CW logo"/>
                        </p>

                    </div>

                    <hr>

                    <div class="rev_slider">
                        <!-- This container should be replaced by Revolution Slider, see https://revolution.themepunch.com/before-after-slider/ -->
                        <img class="aligncenter" src="static/images/cw-logo-slider-placeholder.png" alt="Before after preview"/>
                    </div>

                </div>

            </section>

        </main><!-- #main -->

    </div><!-- #primary --></div><!-- #content -->

	<?php require __DIR__ . '/template-parts/footer.php'; ?>
