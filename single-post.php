<?php
/**
 * Home Template File
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
 */

$context = Timber::get_context();
$context['post'] = Timber::get_post();
$context['hero_image'] = $context['post']->thumbnail();
$templates = array( 'single-post.twig' );
weare502_modify_footer_cta( $context['post'], $context );

Timber::render( $templates, $context );