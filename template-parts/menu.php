<nav id="site-navigation" class="main-navigation" itemscope="" itemtype="https://schema.org/SiteNavigationElement" aria-label="Main navigation">
    <div class="wrap">
        <ul id="primary-menu" class="menu nav-menu" aria-expanded="false">
            <li class="menu-item">
                <a href="/staff.php" data-hover="About Us"><span>We</span></a></li>
            <li class="menu-item">
                <a href="/work.php" data-hover="Clients"><span>Grow</span></a></li>
            <li class="menu-item">
                <a href="/testimonials.php" data-hover="Testimonials"><span>Midwest Brands</span></a></li>
            <li class="menu-item">
                <a href="/values.php" data-hover="Values"><span>Through</span></a></li>
            <li class="menu-item">
                <a href="/story.php" data-hover="Our Story"><span>The Power of Story</span></a></li>
        </ul>

        <div class="main-navigation__cta">
            <span class="main-navigation__label cta__text heading--light">Get in Touch:</span>
            <a href="/contact.php" class="button">Contact Us</a>
        </div>

        <div class="main-navigation__info-block">

            <div class="info-block__row">
                <span class="main-navigation__label">Office Address:</span>
                <span class="info-block__value"> 121 S. 4th Street, Suite 209<br>Manhattan, Ks 66502</span>
            </div>

            <div class="info-block__row info-block__row--last">
                <span class="main-navigation__label">Call Us At:</span>
                <span class="info-block__value">(785) 320-6621</span>
            </div>

        </div>

        <div class="main-navigation__testimonial">
            <p class="inline-quote">“ Their work was exceptional and their attention to detail made every phase of the project seamless. They
                are trustworthy and capable. Whatever the need, I believe they have the talent and capacity to deliver
                results.”</p>
            <span class="quote-credits">Doug Hofbauer, Frontier Farm Credit</span>
        </div>
    </div>
</nav>
