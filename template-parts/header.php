<header id="masthead" class="site-header">

    <div class="site-header__wrap">

        <div class="site-branding">

            <a href="/" class="custom-logo-link" rel="home" itemprop="url">
                <img src="../static/images/502_Logo.jpg" class="custom-logo" alt="502 Media Logo" itemprop="logo">
            </a>

            <p class="site-title"><a href="#" rel="home">502 Media</a></p>

            <p class="site-description">Indescribably Awesome</p>

        </div><!-- .site-branding -->

        <button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false">Menu<span></span></button>

    </div>

</header><!-- #masthead -->
