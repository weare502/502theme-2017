<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>502 Media</title>
	<script src="https://use.typekit.net/pkg3bkv.js"></script>
	<script>try{Typekit.load({ async: true });}catch(e){}</script>
	<link rel="stylesheet" type="text/css" href="https://cloud.typography.com/7203196/6234572/css/fonts.css" />
<!--    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">-->
    <!-- START IMPORT PLUGIN STYLES -->
    <link rel="stylesheet" href="../plugins/gravityforms/formsmain.css">
    <!-- END IMPORT PLUGIN STYLES -->
    <link rel="stylesheet" href="../static/scss/style.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.7.1/slick.min.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.7.1/slick-theme.min.css"/>
</head>

<body class="no-js">

<script type="text/javascript">
    //<![CDATA[
    (function(){
        var c = document.body.classList;
        c.remove('no-js');
        c.add('js');
    })();
    //]]>
</script>

<?php require __DIR__ . '/menu.php'; ?>
