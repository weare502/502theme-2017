<footer id="colophon" class="site-footer">

    <div class="footer__wrap">

        <div class="footer__block footer__block--left">
            <div class="footer__block-wrapper">
                <h4 class="heading heading--light">Back To:</h4>
                <a class="button" href="#">Previous Page</a>
            </div>
        </div>

        <div class="footer__block footer__block--right">
            <div class="footer__block-wrapper">
                <h4 class="heading heading--light">Want To See More?</h4>
                <a class="button" href="#">View Our Work</a>
            </div>
        </div>

    </div><!-- .footer__wrap -->
    <a href="/contact.php" id="get-in-touch" class="flex-container">
        <div class="get-in-touch__text">
            <h5 class="heading heading--orange">Let's Get In Touch</h5>
            <p>We Would Love To Hear Your Story</p>
        </div>
        <div class="get-in-touch__image"></div>
    </a>
</footer><!-- #colophon -->
</div><!-- #page -->

<script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script src="https://unpkg.com/isotope-layout@3/dist/isotope.pkgd.min.js"></script>
<script type="text/javascript" src="./static/js/site.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.7.1/slick.min.js"></script>

</body>

</html>
