<?php
/**
 * Template Name: About Us/Staff
 */

$context = Timber::get_context();
$context['post'] = Timber::get_post();
$context['hero_image'] = $context['post']->thumbnail();
$context['all_staff'] = Timber::get_posts('post_type=staff&posts_per_page=100&orderby=title&order=ASC');
$context['posts'] = Timber::get_posts('posts_per_page=2&post_type=post');
$context['jobs'] = Timber::get_posts('posts_per_page=100&post_type=job');
$templates = array( 'about-us.twig' );

Timber::render( $templates, $context );