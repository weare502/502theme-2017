<?php require __DIR__ . '/template-parts/html-head.php'; ?>

<div id="page" class="site full-width staff-interior">

    <a class="skip-link screen-reader-text" href="#content">Skip Links</a>

    <div class="hero" style="background-image: url('/static/images/christy-chase.jpg');">

        <div class="hero__wrap">

			<?php require __DIR__ . '/template-parts/header.php'; ?>

        </div>

    </div>

    <div id="content" class="site-content"><div id="primary" class="content-area">

        <main id="main" class="site-main">

            <section class="section--full-width" itemscope="" itemtype="https://schema.org/CreativeWork">

                <div class="section__wrap section__wrap--wide">

                    <header class="entry-header">

                        <h1 class="heading heading--main heading--separator-after" itemprop="headline">Meet Christy
                            Chase</h1>

                    </header>

                    <div class="width--70 margin-bottom--6">

                        <p>While the rest of us focus on our own daily grind, Christy's focus is on the bigger picture
                            at 502. She's a joyful spirit that makes sure this is a great place to call home.</p>

                        <p>Organizations - from families to companies - aren't fun when everyone is focused on external
                            things. When no one cares about home base, nothing seems to run smoothly. Christy's role
                            allows her to help 502 by championing the ongoing development and systematic improvement of
                            our team.</p>

                    </div>

                </div>

            </section>

            <section class="section--full-width fun-facts section--background-image background-image--box-shadow" style="background-image:url('/static/images/work-interior-background.png');" itemscope="" itemtype="https://schema.org/CreativeWork">

                <div class="section__wrap">

                    <h2 class="heading heading--separator-after font-size--3">Fun Facts / Biography</h2>

                    <img class="alignright" src="/static/images/fun-facts.jpg" alt="Fun facts"/>

                    <p>Originally from Leonardville, Kansas, Christy graduated from Kansas State University with a
                        degree in Broadcast Journalism. She came to 502 after four years of sales experience with the
                        Manhattan Convention & Visitors Bureau.</p>

                    <p>Clients win because Christy makes sure our team is equipped with what we need to succeed. She
                        makes sure the trains run on time, there
                        's clarity on what needs to be done and that we all have an advocate, making this a great place
                        to work!</p>

                </div>

            </section>

            <section class="section--full-width" itemscope="" itemtype="https://schema.org/CreativeWork">

                <div class="section__wrap section__wrap--wide">

                    <h2 class="heading heading--main heading--separator-after font-size--3">Authored Blog Posts</h2>

                    <p class="width--65">Here at 502, we live and breath stories and have even written a few of our own!
                        Below you will find authored blog posts curated by Christy Chase.</p>

                    <div class="grid grid--2 grid-gutter--30 grid--post-links">

                        <a class="entry grid__item post-link--bg-image" href="/blog.php" style="background-image:url('/static/images/blog-featured-image-1.jpg');">
                            <h3 class="heading heading--light heading--separator-after--white post-title font-size--4">
                                Why Being Distracted At
                                Work Can Help Productivity</h3>
                            <span class="post-term font-size--2">Work Culture</span>
                        </a>

                        <a class="entry grid__item post-link--bg-image" href="/blog.php" style="background-image:url('/static/images/blog-featured-image-2.jpg');">
                            <h3 class="heading heading--light heading--separator-after--white post-title font-size--4">
                                You. Are. Creative.</h3>
                            <span class="post-term font-size--2">Marketing</span>
                        </a>

                    </div>

                    <p class="text-align--center">
                        <a class="button button--outline" href="/blog.php">View All Blog Posts</a>
                    </p>

                </div>

            </section>

        </main><!-- #main -->

    </div><!-- #primary --></div><!-- #content -->

	<?php require __DIR__ . '/template-parts/footer.php'; ?>
