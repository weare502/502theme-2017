;(function ($, undefined) {
    'use strict';

    var body,
        menuToggleButton,
        scrollTo,
        isotopeContainer,
        isotopeMenuButtons,
        viewAllBlogButton,
        blogAuthorButtons,
        blogCategoryButtons,
        homeTypewriterElement,
        homeOpenMenuButton;

    var init = {
        setSelectors: function () {
            body = $('body');
            menuToggleButton = $('.menu-toggle');
            scrollTo = $('.scroll-to');
            isotopeContainer = $('.masonry-grid');
            isotopeMenuButtons = $('#isotope-filters a');
            viewAllBlogButton = $('#view-all-posts-button');
            blogAuthorButtons = $('#blog-authors li a');
            blogCategoryButtons = $('#blog-categories li a');
            homeTypewriterElement = $('#typewriter');
            homeOpenMenuButton = $('.home .open-menu.button');
        },

        isotopeGallery: function() {
            if ( undefined === isotopeContainer ) {
                return;
            }

            this.isotopeColumnWidth();
        },

        isotopeColumnWidth: function() {
            isotopeContainer.isotope({
                // options...
                resizable: true,
                // percentPosition: true,
                // set columnWidth to a percentage of container width
                masonry: {
                    columnWidth: '.masonry-grid__grid-item',
                    gutter: 30
                }
            });
            window.setTimeout(function(){
                isotopeContainer.isotope('reloadItems');
                isotopeContainer.isotope('layout');
            }, 2000);
        },

        slickSlider: function() {
            $('.testimonials-slider').slick();
        }
    };

    var view = {
        addClickListeners: function () {
            if (undefined !== menuToggleButton) {
                menuToggleButton.on('click', this.toggleMenu);
                menuToggleButton.on('click', this.toggleButtonText);
            }

            if (undefined !== homeOpenMenuButton) {
                homeOpenMenuButton.on('click', this.toggleMenu);
                homeOpenMenuButton.on('click', this.toggleButtonText);
            }


            if (undefined !== scrollTo) {
                scrollTo.on('click', this.scrollTo);
            }

            if (undefined !== isotopeMenuButtons ) {
                isotopeMenuButtons.on('click', this.toggleIsotopeButtonClass);
                isotopeMenuButtons.on('click', this.filterIsotopeItems)
                $(window).scroll(function() {
                    var x = $(this).scrollTop();
                    $('.our-work .hero').css('background-position', parseInt(-x / 5) + 'px' + ' 0%');
                    isotopeContainer.isotope('layout');
                });
            }

            if (undefined !== viewAllBlogButton ) {
                viewAllBlogButton.on('click', this.showAllBlogPosts);
            }

            if ( undefined !== blogAuthorButtons ) {
                blogAuthorButtons.on('click', this.showAuthoredBlogPosts);
            }

            if ( undefined !== blogCategoryButtons ) {
                blogCategoryButtons.on('click', this.showCategorizedBlogPosts);
            }

            if ( homeTypewriterElement.length ) {
                var options = {
                  strings: ["Creative", "Deliberate", "Strategic"],
                  typeSpeed: 50,
                  backSpeed: 50,
                  startDelay: 200,
                  backDelay: 1000,
                }

                var typed = new Typed("#typewriter", options);
            }
        },

        toggleMenu: function () {
            body.toggleClass('menu-activated');
            $('html').toggleClass('menu-activated');
            menuToggleButton.toggleClass('activated');
        },

        toggleButtonText: function () {
            if ('Menu' === menuToggleButton.text()) {
                menuToggleButton.text('Close');
            } else {
                menuToggleButton.text('Menu');
            }

            // Required for the CSS button icon.
            menuToggleButton.append('<span>');
        },

        scrollTo: function (element) {
            element.preventDefault();

            var linkTo = $(this).attr('href');

            body.animate({
                scrollTop: $(linkTo).offset().top - 30
            }, 700);
        },

        toggleIsotopeButtonClass: function() {
            $(this).addClass('button--current-item');
            $(isotopeMenuButtons).not(this).removeClass('button--current-item');
        },

        filterIsotopeItems: function() {
            var selector = $(this).attr('data-filter');
            isotopeContainer.isotope({ filter: selector });
            return false;
        },

        showAllBlogPosts: function() {
            $('#content .entry.hidden').removeClass('hidden');
            $('#content .entry').show();
            viewAllBlogButton.hide();
        },

        showAuthoredBlogPosts: function(e) {
            e.preventDefault();
            $('#blog-authors .current-item').removeClass('current-item');
            $('#blog-categories .current-item').removeClass('current-item');
            view.showAllBlogPosts();
            var author = $(this).text();
            $(this).addClass('current-item');
            $('#content .entry').hide();
            $('[data-author="' + author + '"]').show();
            if ( author.toLowerCase() === 'all authors' ) {
                view.showAllBlogPosts();
            }
            body.animate({
                scrollTop: $('#content').offset().top - 30
            }, 700);
        },

        showCategorizedBlogPosts: function(e) {
            e.preventDefault();
            $('#blog-categories .current-item').removeClass('current-item');
            $('#blog-authors .current-item').removeClass('current-item');
            view.showAllBlogPosts();
            var category = $(this).text();
            $(this).addClass('current-item');
            $('#content .entry').hide();
            $('[data-category="' + category + '"]').show();
            if ( category.toLowerCase() === 'all' ) {
                view.showAllBlogPosts();
            }
            body.animate({
                scrollTop: $('#content').offset().top - 30
            }, 700);
        }
    };

    $(document).ready(function () {
        init.setSelectors();
        view.addClickListeners();
        init.slickSlider();
        init.isotopeGallery();

        $('body').fitVids();

        $(window).load(function(){
            isotopeContainer.isotope('layout');
        });
    });

    $(window).load(function(){
        window.sr = ScrollReveal();
        sr.reveal('.grid__item');
        sr.reveal('.grid__item.staff-profile', 400);
        sr.reveal('.masonry-grid__grid-item');
        sr.reveal('.section--testimonials-slider');
        sr.reveal('.testimonials__wrap section:first-child');
        sr.reveal('.contact-form .gfield', 100);
        
        sr.reveal('.testimonials__wrap .section--half-width', {
            origin: 'left',
            distance: '50%',
            scale: 1
        });
        sr.reveal('.testimonials__wrap .section--testimonial-bg', {
            origin: 'right',
            distance: '50%',
            scale: 1
        });
        sr.reveal('.industry__image', {
            origin: 'left',
            distance: '50%',
            scale: 1
        });
        sr.reveal('.industries-wrap',{
            origin: 'right',
            distance: '50%',
            scale: 1
        });
        sr.reveal('.entry-header--boxed', 
            {
                origin: 'right',
                distance: '50%',
                scale: 1
            } 
        );
        sr.reveal('.custom-logo', {
            origin: 'top',
            distance: '50%',
            scale: 1
        });
        sr.reveal('button.menu-toggle', {
            origin: 'top',
            distance: '100%',
            scale: 1
        });

        sr.reveal('.footer__block--left', {
            origin: 'left',
            scale: 1,
            viewFactor: 0.5,
            distance: '100%'
        });
        sr.reveal('.footer__block--right', {
            origin: 'right',
            scale: 1,
            viewFactor: 0.5,
            distance: '100%'
        });

        sr.reveal('.footer-contact', {
            origin: 'bottom',
            scale: 1,
            viewFactor: 0.5,
            distance: '100%'
        });

        sr.reveal('.reveal-slide-in-right', {
            origin: 'right',
            scale: 1,
            viewFactor: 0.5,
            distance: '100%'
        });

        sr.reveal('.reveal-slide-in-left', {
            origin: 'left',
            scale: 1,
            viewFactor: 0.5,
            distance: '100%'
        });

        sr.reveal('.reveal-slide-in-bottom', {
            origin: 'bottom',
            scale: 1,
            viewFactor: 0.5,
            distance: '100%'
        });
        // sr.reveal('.section--full-width')
        
        $(window).scroll(function() {    
            var scroll = $(window).scrollTop();

            if (scroll >= 500) {
                $("#get-in-touch").addClass("visible");
            } else {
                $("#get-in-touch").removeClass("visible");
            }
        });
    });

})(jQuery, undefined)
