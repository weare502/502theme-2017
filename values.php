<?php require __DIR__ . '/template-parts/html-head.php'; ?>

<div id="page" class="site our-values full-width">

    <a class="skip-link screen-reader-text" href="#content">Skip Links</a>

    <div class="hero" style="background-image: url('/static/images/values-hero-bg.jpg');">

        <div class="hero__wrap">

			<?php require __DIR__ . '/template-parts/header.php'; ?>

            <header class="entry-header entry-header--boxed">

                <h1 class="entry-title heading heading--light" itemprop="headline">Our Approach</h1>

                <p class="entry-subtitle entry-subtitle--light" itemprop="tagline">Values That Define 502</p>

            </header>

        </div>

    </div>

    <div id="content" class="site-content">

        <div id="primary" class="content-area">

            <main id="main" class="site-main">

                <section id="value-1" class="section--values section--background-image flex-container" style="background-image: url('/static/images/values-1-bg.jpg');" itemscope="" itemtype="https://schema.org/CreativeWork">
                    <div class="value__wrap">
                        <h2 class="heading heading--orange font-size--6">1. Be Irrationally Generous</h2>
                        <p>Lead the way with irrational generosity.  <br>Truly believe that it is better to give than to
                            receive.</p>
                    </div>
                </section>

                <section id="value-2" class="section--values section--background-image flex-container" style="background-image: url('/static/images/values-2-bg.jpg');" itemscope="" itemtype="https://schema.org/CreativeWork">
                    <div class="value__wrap">
                        <h2 class="heading heading--orange font-size--6">2. Own It</h2>
                        <p>A commitment of the head, heart and hands to fix the problem and never affix the blame.</p>
                    </div>
                </section>

                <section id="value-3" class="section--values section--background-image flex-container" style="background-image: url('/static/images/values-3-bg.jpg');" itemscope="" itemtype="https://schema.org/CreativeWork">
                    <div class="value__wrap">
                        <h2 class="heading heading--light font-size--6">3. Make It Better</h2>
                        <p>Solve problems, challenge solutions, compete against yesterday.</p>
                    </div>
                </section>

                <section id="value-4" class="section--values section--background-image flex-container" style="background-image: url('/static/images/values-4-bg.jpg');" itemscope="" itemtype="https://schema.org/CreativeWork">
                    <div class="value__wrap">
                        <h2 class="heading heading--light font-size--6">4. Work As A Team</h2>
                        <p>Our work is better when it is the result of collaboration. <br>Never be too proud to ask for help
                            or too busy to offer it.</p>
                    </div>
                </section>

                <section id="value-5" class="section--values section--background-image flex-container" style="background-image: url('/static/images/values-5-bg.jpg');" itemscope="" itemtype="https://schema.org/CreativeWork">
                    <div class="value__wrap">
                        <h2 class="heading heading--light font-size--6">5. Establish Clarity</h2>
                        <p>Seek and provide clear understanding. <br>Clarity is kindness.</p>
                    </div>
                </section>

                <section id="value-6" class="section--values section--background-image flex-container" style="background-image: url('/static/images/values-6-bg.jpg');" itemscope="" itemtype="https://schema.org/CreativeWork">
                    <div class="value__wrap">
                        <img src="/static/images/values-6-img.png" alt="Be Respectful. You might not always agree with everyone but everyone always deserves respect.">
                    </div>
                </section>

                <section id="value-7" class="section--values section--background-image flex-container" style="background-image: url('/static/images/values-7-bg.jpg');" itemscope="" itemtype="https://schema.org/CreativeWork">
                    <div class="value__wrap">
                        <h2 class="heading heading--orange font-size--6">7. Stay Fit</h2>
                        <p>Our personal lives come to work with us. The more responsibility you have in an organization,
                            the more important it is for you to stay fit - mentally, physically, financially,
                            spiritually, and relationally.</p>
                    </div>
                </section>

            </main><!-- #main -->

        </div><!-- #primary -->

    </div><!-- #content -->

	<?php require __DIR__ . '/template-parts/footer.php'; ?>
